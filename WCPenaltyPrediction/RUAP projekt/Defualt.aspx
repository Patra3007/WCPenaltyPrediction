﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Defualt.aspx.cs" Inherits="RUAP_projekt.Defualt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset ="UTF-8">
     <title>Penalty Prediction</title>
     <link rel="stylesheet" type="text/css" href="main.css">
     <script src="main.js" type="text/javascript"></script>
</head>
<body>
     <form id="form1" runat="server">
        <header>
            <p>
                <strong>Football World Cup Penalty Prediction</strong> <img style="height:48px; width: 48px;" src="images/Soccer_ball_animated.svg.png">
            </p>
            
        </header>


        <div class="row">
            <div class="column">
                <p id="Shooter">
                    <strong>Shooter information</strong>
                    <br>  <br> 
                         Stronger leg &emsp; &emsp;
                         <select name="leg" id="leg" class="sel">
                             <option value="L">Left</option>
                             <option value="R">Right</option>
                         </select>
                </p>
                <br>  <br> 
                <p id="factors">
                    <strong>Additional factors</strong>   
                        <br>  <br> 
            
                    Shot on target
                    &emsp; &emsp;
                    <select name="onTarget" id="onTarget" class="sel">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                      </select>
                      <br> <br> 
                    Number of penalty
                    &emsp; &emsp;
                    <select name="number" id="number" class="sel">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </select>
                      <br> <br>
                    Eliminational
                    &emsp; &emsp;
                    <select name="elim" id="elim" class="sel">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                </p>
            </div>
            <div class="column">
                <p id="Pictures">
                <strong>Chosoe a place where the goaly will try to make a save!</strong>

                    
                    <br>
                    <img runat="server" style=" cursor: pointer;" id="golay1" src="images/goaly1.jpg" alt="Left side of the goal" onclick="mark_save(this,'L')">
                    <img runat="server" style=" cursor: pointer;" id="golay2" src="images/goaly2.jpg" alt="Middle side of the goal" onclick="mark_save(this,'M')">
                    <img  runat="server" style=" cursor: pointer;" id="golay3" src="images/goaly3.jpg" alt="Right side of the goal" onclick="mark_save(this,'R')">
                    
                </p>
        
                <p id="Pictures">
                    <strong>Choose a place where the shooter will shoot!</strong>
                    
                    <br>
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target1" src="images/target1.jpg" onclick="mark_shot(this,'1')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target2" src="images/target2.jpg" onclick="mark_shot(this,'2')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target3" src="images/target3.jpg" onclick="mark_shot(this,'3')">
                    <br>
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target4" src="images/target4.jpg" onclick="mark_shot(this,'4')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target5" src="images/target5.jpg" onclick="mark_shot(this,'5')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target6" src="images/target6.jpg" onclick="mark_shot(this,'6')">
                    <br>
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target7" src="images/target7.jpg" onclick="mark_shot(this,'7')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target8" src="images/target8.jpg" onclick="mark_shot(this,'8')">
                    <img runat="server" style="height:62px; width: 158px; cursor: pointer;" id="target9" src="images/target9.jpg" onclick="mark_shot(this,'9')">
                    
                    
                </p>
            </div>     
        </div> 
        <asp:LinkButton runat="server" id = "predicter" OnClick="lbPredict_Click">Predict Scoring</asp:LinkButton>
         <br>  <br> 
          <br>  <br>
          <br>  <br>
        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
         <asp:Label ID="Label1" runat="server" Text=""></asp:Label>

         <asp:HiddenField ID="zone" runat="server" value="1"/>
         <asp:HiddenField ID="goalkeeper" runat="server" value="L"/>
 
         </form>
            <script type="text/javascript" src="{% static '/js/main.js' %}" ></script>
</body>
</html>

