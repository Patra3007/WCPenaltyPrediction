﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace RUAP_projekt
{
    public partial class Defualt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
 
        }

        protected void lbPredict_Click(object sender, EventArgs e)
        {
            var options = new RestClientOptions("https://ussouthcentral.services.azureml.net")
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest("/workspaces/49bff328fa0c4e99bb34c65d7863b85c/services/1429936cdcd2408b8784e8bce6a691de/execute?api-version=2.0&format=swagger", Method.Post);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer JP41ZbqCVTU7WLiQMW54rkqnZcVuTiSTLUpjL614RZSV+Uc6C9Sk+KMJYFfk1hTctnV6h/QCgl3m+AMCJciVgw==");
            string leg = Request.Form["leg"];
            string onTarget = Request.Form["onTarget"];
            string number = Request.Form["number"];
            string elim = Request.Form["elim"];
            var zone1 = zone.Value;
            string goalkeeper1 = goalkeeper.Value;

            var body = @"{" + "\n" +
            @"    ""Inputs"": {" + "\n" +
            @"        ""input1"": [" + "\n" + @"  {" + "\n" + @"" + "\n" +
            @"                            ""Game_id"": """",   " + "\n" + @"" + "\n" +
            @"                            ""Team"":  """",   " + "\n" + @"" + "\n" +
            @"                            ""Zone"": ""#zone#"",   " + "\n" + @"" + "\n" +
            @"                            ""Foot"": ""#leg#"",     " + "\n" + @"" + "\n" +
            @"                            ""Keeper"": ""#goalkeeper#"",   " + "\n" + @"" + "\n" +
            @"                            ""OnTarget"": ""#onTarget#"",   " + "\n" + @"" + "\n" +
            @"                            ""Goal"": ""1"",   " + "\n" + @"" + "\n" +
            @"                            ""Penalty_Number"": ""#number#"",   " + "\n" + @"" + "\n" +
            @"                            ""Elimination"": ""#elim#""" + "\n" + @"            }" + "\n" + @"        ]," + "\n" + @"    }," + "\n" +
            @"    ""GlobalParameters"": {}" + "\n" + @"}";
            var result1 = body.Replace("#leg#", leg).Replace("#onTarget#", onTarget).Replace("#number#", number).Replace("#elim#", elim).
                                Replace("#zone#", zone1).Replace("#goalkeeper#", goalkeeper1);
            request.AddStringBody(result1, DataFormat.Json);
            RestResponse response = client.Execute(request);
            //lblResults.Text = response.Content.ToString();
            var results = JObject.Parse(response.Content);
            string prediction = results["Results"]["output1"].ToString();
            prediction = prediction.Replace("[", "").Replace("]", "").Replace("\"", "").Replace(" ", "").Replace("}", "");
            if(number == "10" || number == "11" || number == "12") {
                prediction = prediction.Substring(129);
            }
            else
            {
                prediction = prediction.Substring(128);
            }

            float predict = float.Parse(prediction, CultureInfo.InvariantCulture.NumberFormat);
            if(predict > 0.5)
            {
                Label1.Text = "It's a goallllllll!";
            }
            else
            {
                Label1.Text = "Missed! :(";
            }
        }
    }
}