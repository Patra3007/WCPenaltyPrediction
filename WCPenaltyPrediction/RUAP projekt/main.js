var InputButton = document.querySelector('inputButton')
var Team;
var Foot;
var OnTarget;
var Penalty_Number;
var Elimination;

var selected_items_save = 0;
var selected_items_shot = 0;
var target_save;
var target_shot;



function mark_save(target, Keeper) {
    if (selected_items_save < 1) {
        target.style.border = "5px solid #5c6664";
        selected_items_save += 1;
        target_save = target;
      }
    else{
        if(target == target_save){
        target_save.style.border = "";
        selected_items_save = 0;
        }
        else{
        target_save.style.border = "";
        target.style.border = "5px solid  #5c6664";
        target_save = target;
        }
    }
    document.getElementById("goalkeeper").value = Keeper;
}

function mark_shot(target, Zone) {
    if (selected_items_shot < 1) {
        target.style.height = "52px";
        target.style.width = "148px";
        target.style.border = "5px solid  #5c6664";
        selected_items_shot += 1;
        target_shot = target;
        
      }
    else{
        if(target == target_shot){
        target_shot.style.height = "62px";
        target_shot.style.width = "158px";
        target_shot.style.border = "";
        selected_items_shot = 0;
        }
        else{
        target_shot.style.height = "62px";
        target_shot.style.width = "158px";
        target_shot.style.border = "";
        target.style.height = "52px";
        target.style.width = "148px";
        target.style.border = "5px solid  #5c6664";
        target_shot = target;
        }
    }
    document.getElementById("zone").value = Zone;
}
